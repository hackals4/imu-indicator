classdef CSP < handle
    %% ===================== DESCRIPTION ==================================
    % This object performs Common Spatial Patterns (CSP) method to find
    % the transformation matrix for a data set of two classes to maximize
    % the ratio of signal variances of these classes. To reduce the
    % dimensionality of the data set according to a level of reduction
    % determined by the user, a specific number of most informative filters
    % extracted from the obtained transformation matrix is used for
    % filtering the data.
    %
    % METHODS:
    %   Learn - performs CSP to learn the desired spatial filters
    %   (transformation matrix) for the given input dataset.
    %   Reduce - reduces dimensionality based on the number of features
    %   determined by the user.
    
    %% ============================ PROPERTIES ============================
    properties
        
        % 0-1 flag for normalization of the covariance matrices by the trace
        normalize
        
        % scalar, number of features to reduce the dimensionality to
        nFeatures
        
        % transformation matrix with projection weights (spatial filters)
        eigenVectors
        
        % row vector with measures of the quality of spatial filters (i.e.,
        % eigenvalues)
        filterInformation
        
    end
    
    methods
        %% ============================ CONSTRUCTOR =======================
        function self = CSP(varargin)
            
            p = inputParser;
            p.addParameter('normalize',true,@islogical);
            p.addParameter('nFeatures',2,@isscalar);
            p.parse(varargin{:});
            
            self.normalize = p.Results.normalize;
            self.nFeatures = p.Results.nFeatures;
            
        end
        
        %% ========================= LEARN ================================
        function Learn(self,inputData)
            % learns the desired spatial filters for the given input dataset.
            % INPUT
            %   inputData = {numClasses}[numChannels x numSamples x numTrials]
            
            properChannels = true;
            for cl = 2:length(inputData)
                if size(inputData{cl-1},1)~=size(inputData{cl},1)
                    properChannels = false;
                end
            end
            
            if ~properChannels
                error('Number of channels of the classes are not the same!');
            else
                nChannels = size(inputData{1},1);
                nClasses = length(inputData);
                
                % compute normalized spatial class covariance matrices
                CC = zeros(nChannels,nChannels,nClasses);
                for cl = 1:nClasses
                    E = inputData{cl};
                    E = E(:,:);
                    E = bsxfun(@minus, E, mean(E,2));
                    if self.normalize
                        CC(:,:,cl) = E*E'/(size(inputData{cl},2)*trace(E*E'));
                    elseif self.normalize
                        CC(:,:,cl) = E*E'/size(inputData{cl},2);
                    end
                    CC(:,:,cl) = CC(:,:,cl)/size(inputData{cl},3);
                end
                
                % apply two-class CSP
                % compute spatial transformation matrix
                [self.eigenVectors,self.filterInformation] = eig(CC(:,:,1),CC(:,:,1)+CC(:,:,2));
                self.filterInformation = diag(self.filterInformation)';
                self.eigenVectors = bsxfun(@rdivide, self.eigenVectors, sqrt(sum(self.eigenVectors.^2,1)));
                
                for ei = 1:size(self.eigenVectors,2)
                    if self.eigenVectors(1,ei)<0
                        self.eigenVectors(:,ei) = -self.eigenVectors(:,ei);
                    end
                end
            end
        end
        
        %% ========================= REDUCE ===============================
        function outputData = Reduce(self,inputData)
            % reduces the dimensionality of the data set using the columns
            % of the transformation matrix, according to the number of
            % features provided by the user. It assumes learn method has
            % been called previously.
            % INPUT
            %   inputData = {numClasses}[numChannels x numSamples x numTrials]
            %
            % OUTPUT
            %   outputData = {numClasses}[nFeatures x numSamples x numTrials]
            
            if(self.nFeatures > size(inputData{1},1))
                error('Provide the parameter nFeatures smaller than numChannels!');
            else
                % Make into cell
                if ~iscell(inputData)
                    inputData = {inputData};
                end
                
                L = self.nFeatures;
                V = self.eigenVectors;
                    if(mod(L,2)~=0)
                        error('Provide the parameter nFeatures as a multiple of 2 as nFeatures leftmost and rightmost columns of the transformation matrix will be selected as filters.');
                    else
                        w = V(:,end+1-L/2:end);
                        w = cat(2,w,V(:,1:L/2));
                    end
                
                outputData = cell(length(inputData),1);
                for cl = 1:length(inputData)
                    for tr = 1:size(inputData{cl},3)
                        E = inputData{cl}(:,:,tr);
                        outputData{cl} = cat(3,outputData{cl},w'*E);
                    end
                end
            end
        end
    end
end