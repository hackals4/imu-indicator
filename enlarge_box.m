function enlarge_box(h)
set(h, 'position', [200 200 1000 100]); %makes box bigger
ah = get( h, 'CurrentAxes' );
ch = get( ah, 'Children' );
set( ch, 'FontSize', 64 ); %makes text bigger
end