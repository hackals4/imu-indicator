classdef model < handle
    
    %% ============================ PROPERTIES ============================
    properties
        
        num_features
        
        reducer
        
        classifier
        
    end
 
    methods
        %% ============================ CONSTRUCTOR =======================
        function self = model(varargin)
            
            p = inputParser;
            p.addParameter('nFeatures',2,@isscalar);
            p.parse(varargin{:});
            
            self.num_features = p.Results.nFeatures;
            self.reducer = CSP('nFeatures',self.num_features);
            self.classifier = [];%% TODO: LDA
            
        end

        function learn_dimensions(data, labels)
            % train the reducer
            train_set = self.format_train_csp(data, labels);
            self.reducer.Learn(train_set);
        end
        
        function data_red = reduce_dimensions(data, labels)
            % apply the reducer
            data_set = self.format_train_csp(data, labels);
            data_red = self.reducer.Reduce(data_set);
        end
        
        function extract_features(
    
end