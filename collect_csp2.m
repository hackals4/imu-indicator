%clear all; clc

%% Establish Connection
%connector on;
%dev = mobiledev;
dev.discardlogs;

% disp('Set Android Device to send Accelerometer data then press return:'); pause

% dev.SampleRate = 10; % This is bugged if you try to do it in a script
% disp('Manually set sample rate e.g. dev.SampleRate=10.'); keyboard;

num_classes = 2;
num_channels = 3;

%% Parameters
num_trials_per_class = 10;
num_trials = num_trials_per_class*num_classes;
trial_len_s = 3;

labels = [];
for c = 1:num_classes
    labels = vertcat(labels, c*ones(num_trials_per_class,1));
end
labels = labels(randperm(length(labels)));

%% Collect and plot 3-Seconds of Accelerometer Data
ns_collect = trial_len_s*dev.SampleRate;
training_collect = zeros(num_channels, ns_collect, num_trials);
training_data = cell(num_classes,1);

% Wait for Data
h = msgbox('Waiting for data!', 'Android Connection');%, 'error');
enlarge_box(h);
while (length(dev.accellog) < ns_collect)
    pause(1/dev.SampleRate);
end
close(h);

h = msgbox('Do not move!', 'Trial 0');%, 'error');
enlarge_box(h);
pause(trial_len_s);

for t = 1:num_trials
    
    % Collect trial
    close(h);
    clc;disp(labels);
    if (labels(t) == 1)
        h = msgbox('Do not move!', ['Trial ' num2str(t)]);%, 'error');
        enlarge_box(h);
    else
        h = msgbox('Move now!', ['Trial ' num2str(t)]);%, 'warn');
        enlarge_box(h);
    end
    pause(trial_len_s);
    
    % Retrieve the data from the log
    accel_data = dev.accellog;
    dats = accel_data(end-ns_collect+1:end,:);
    
    % Plot the trial data
    %figure(1); plot3(dats(:,1),dats(:,2),dats(:,3));
    %xlabel('X');ylabel('Y');zlabel('Z');
    %title([num2str(t) 'th trial of Accelerometer Measurements - Class=' num2str(labels(t))]);

    % Store the trial data
    training_collect(:,:,t) = dats';
    
    % Pause between trials
    close(h);
    h = msgbox('Do not move!', 'Break');%, 'error');
    enlarge_box(h);
    pause(trial_len_s)
end

close(h);

% Reshape the trial data into the desired format
for c = 1:num_classes
    training_data{c} = training_collect(:,:,labels == c);
end

% Train the CSP
my_csp = CSP();
my_csp.Learn(training_data);

% Reduce the training data using CSP and extract features
training_red = my_csp.Reduce({training_collect});
training_red = training_red{:};
feats = zeros(num_trials,2);
for t = 1:num_trials
    feats(t,:) = var(training_red(:,:,t),0,2)';
end
scatter(feats(:,1), feats(:,2), 64, labels, 'filled');

% Compute trial variances and train classifier
model = fitcdiscr(feats,labels);

% Reset the buffer
dev.discardlogs;

% Save trained system
save('trained_imu_classifier.mat')

%load('trained_imu_classifier.mat')

%% Continuous Prediction Loop
n = 0;
prev_class = 1;
while(1)
    n = n + 1;
   	% Wait for Data
    while (length(dev.accellog) < ns_collect)
        pause(1/dev.SampleRate);
    end
    
    % Retrieve the data from the log
    accel_data = dev.accellog;
    dats = accel_data(end-ns_collect+1:end,:);
    
    test_red = my_csp.Reduce({dats'});
    test_red = test_red{:};
    
    test_feat = var(test_red,0,2)';
    
    predicted_class = model.predict(test_feat);
    
    if (predicted_class == 2) && (prev_class == 2)
        h = msgbox(['You have sent the On signal!'], 'Prediction');%, 'error');
        enlarge_box(h);
    else
        h = msgbox(['You the signal is NOT on!'], 'Prediction');%, 'error');
        enlarge_box(h);
    end
    
    % Plot the trial data in feature space
    figure(1); scatter([feats(:,1); test_feat(1)], ...
        [feats(:,2); test_feat(2)], 64, ...
        [labels; predicted_class], 'filled');
    xlabel('Feature 1');ylabel('Feature 2');
    title(['Test ' num2str(n) ' - Predicted Class=' num2str(predicted_class)]);

    prev_class = predicted_class;
    pause(trial_len_s);
    close(h);
end