# README #

To run, you must first install and configure the Android matlab application to communicate with the computer where your matlab location is indicated.
Then, execute the following command in your matlab to finish configuration

dev = mobiledev

Finally, press the "Start Sending" button in the matlab application on your Android device and run the collect_csp2.m script.

TODO: Split the collect_csp2.m script into two parts: one which collects and saves the trained classifiers, and one which runs the trained classifier in a loop on new data.